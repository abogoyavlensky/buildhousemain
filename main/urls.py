__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'


from django.conf.urls import patterns, url

# from views import Main, Contact, ObjectList, ObjectDetail
from views import Main

urlpatterns = patterns('main.views',
    url(r'^$', Main.as_view(), name='main'),
    # url(r'^contact/', Contact.as_view(), name='contact'),

    # url(r'^offers/(?P<offer_type>[a-z]+)/(?P<object_type>[a-z]+)/$',
    #     ObjectList.as_view(), name='object_list'),

    # url(r'^offers/(?P<offer_type>[a-z]+)/(?P<object_type>[a-z]+)/(?P<pk>\d+)/$',
    #     ObjectDetail.as_view(), name='object_detail'),
)

