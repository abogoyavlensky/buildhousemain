__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from main import views
from django.conf import settings


def get_settings(request):
    _settings = {
        # 'offer_types': views.OFFER_TYPES,
        # 'default_object_type': settings.DEFAULT_OBJECT_TYPE,
    }
    return {'django_settings': _settings}
